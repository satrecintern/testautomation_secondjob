describe('PetClinic2', () => {
    context('Last name 검색 기능(검색자동화)', () => {
      it('Last name을 입력하고 Find Owner를 클릭', () => {
        cy.visit('http://localhost:8080/');
        cy.get('li:nth-child(2) span:nth-child(2)').click();
        cy.url().should('contains', 'http://localhost:8080/owners/find');
        cy.get('#lastName').click();
        cy.get('#lastName').type('Davis');
        cy.get('.btn:nth-child(1)').click();
        //cy.get('#search-owner-form').submit();
        cy.url().should('contains', 'http://localhost:8080/owners');
});
});
});