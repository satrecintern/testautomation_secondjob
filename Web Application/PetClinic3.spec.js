describe('PetClinic3', () => {
    context('Owner 개인 정보 수정 기능(개인정보수정자동화)', () => {
      it('Owner의 name, address, city 등을 수정한 후 update', () => {
        cy.visit('http://localhost:8080/');
        cy.get('li:nth-child(2) span:nth-child(2)').click();
        cy.url().should('contains', 'http://localhost:8080/owners/find');
        cy.get('#lastName').click();
        cy.get('#lastName').type('Black');
        cy.get('.btn:nth-child(1)').click();
        //cy.get('#search-owner-form').submit();
        cy.url().should('contains', 'http://localhost:8080/owners/7');
        cy.get('.btn:nth-child(3)').click();
        cy.url().should('contains', 'http://localhost:8080/owners/7/edit');
        cy.get('#firstName').click();
        cy.get('#firstName').type('{selectall}');
        cy.get('#firstName').type('{backspace}');
        cy.get('#firstName').type('Jeffry');
        cy.get('#address').click();
        cy.get('#address').type('{selectall}');
        cy.get('#address').type('{backspace}');
        cy.get('#address').type('13548 street');
        cy.get('#city').click();
        cy.get('#city').type('{selectall}');
        cy.get('#city').type('{backspace}');
        cy.get('#city').type('busan');
        cy.get('#telephone').click();
        cy.get('#telephone').type('{selectall}');
        cy.get('#telephone').type('{backspace}');
        cy.get('#telephone').type('984651352');
        cy.get('.btn').click();
        //cy.get('#add-owner-form').submit();
        cy.url().should('contains', 'http://localhost:8080/owners/7');
        
});
});
});