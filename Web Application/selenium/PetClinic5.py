# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re

class PetClinic5(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(30)
        self.base_url = "https://www.google.com/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_pet_clinic5(self):
        driver = self.driver
        driver.get("http://localhost:8080/")
        driver.find_element_by_xpath("//div[@id='main-navbar']/ul/li[2]/a/span[2]").click()
        driver.find_element_by_id("lastName").click()
        driver.find_element_by_id("lastName").clear()
        driver.find_element_by_id("lastName").send_keys("bbb")
        driver.find_element_by_xpath("//button[@type='submit']").click()
        driver.find_element_by_xpath("//a[contains(text(),'Add\n      New Pet')]").click()
        driver.find_element_by_id("name").click()
        driver.find_element_by_id("name").clear()
        driver.find_element_by_id("name").send_keys("hi1")
        driver.find_element_by_id("birthDate").click()
        driver.find_element_by_id("birthDate").clear()
        driver.find_element_by_id("birthDate").send_keys("2000-01-01")
        driver.find_element_by_xpath("//button[@type='submit']").click()
        driver.find_element_by_xpath("//a[contains(text(),'Add\n      New Pet')]").click()
        driver.find_element_by_id("name").click()
        driver.find_element_by_id("name").clear()
        driver.find_element_by_id("name").send_keys("hi2")
        driver.find_element_by_id("birthDate").click()
        driver.find_element_by_id("birthDate").clear()
        driver.find_element_by_id("birthDate").send_keys("2000-01-02")
        driver.find_element_by_id("type").click()
        Select(driver.find_element_by_id("type")).select_by_visible_text("cat")
        driver.find_element_by_id("type").click()
        driver.find_element_by_xpath("//button[@type='submit']").click()
        element=driver.find_element_by_xpath("//a[contains(text(),'Add\n      New Pet')]")
        driver.execute_script("arguments[0].click();", element)
        driver.find_element_by_id("name").click()
        driver.find_element_by_id("name").clear()
        driver.find_element_by_id("name").send_keys("hi3")
        driver.find_element_by_id("birthDate").click()
        driver.find_element_by_id("birthDate").clear()
        driver.find_element_by_id("birthDate").send_keys("2000-01-03")
        driver.find_element_by_id("type").click()
        Select(driver.find_element_by_id("type")).select_by_visible_text("dog")
        driver.find_element_by_id("type").click()
        driver.find_element_by_xpath("//button[@type='submit']").click()
        element=driver.find_element_by_xpath("//a[contains(text(),'Add\n      New Pet')]")
        driver.execute_script("arguments[0].click();", element)
        driver.find_element_by_id("name").click()
        driver.find_element_by_id("name").clear()
        driver.find_element_by_id("name").send_keys("hi4")
        driver.find_element_by_id("birthDate").click()
        driver.find_element_by_id("birthDate").clear()
        driver.find_element_by_id("birthDate").send_keys("2000-01-04")
        driver.find_element_by_id("type").click()
        Select(driver.find_element_by_id("type")).select_by_visible_text("hamster")
        driver.find_element_by_id("type").click()
        driver.find_element_by_xpath("//button[@type='submit']").click()
        element=driver.find_element_by_xpath("//a[contains(text(),'Add\n      New Pet')]")
        driver.execute_script("arguments[0].click();", element)
        driver.find_element_by_id("name").click()
        driver.find_element_by_id("name").clear()
        driver.find_element_by_id("name").send_keys("hi5")
        driver.find_element_by_id("birthDate").click()
        driver.find_element_by_id("birthDate").clear()
        driver.find_element_by_id("birthDate").send_keys("2000-01-05")
        driver.find_element_by_id("type").click()
        Select(driver.find_element_by_id("type")).select_by_visible_text("lizard")
        driver.find_element_by_id("type").click()
        driver.find_element_by_xpath("//button[@type='submit']").click()
        element=driver.find_element_by_xpath("//a[contains(text(),'Add\n      New Pet')]")
        driver.execute_script("arguments[0].click();", element)
        driver.find_element_by_id("name").click()
        driver.find_element_by_id("name").clear()
        driver.find_element_by_id("name").send_keys("hi6")
        driver.find_element_by_id("birthDate").click()
        driver.find_element_by_id("birthDate").clear()
        driver.find_element_by_id("birthDate").send_keys("2000-01-06")
        driver.find_element_by_id("type").click()
        Select(driver.find_element_by_id("type")).select_by_visible_text("snake")
        driver.find_element_by_id("type").click()
        driver.find_element_by_xpath("//button[@type='submit']").click()
        element=driver.find_element_by_xpath("//a[contains(text(),'Add\n      New Pet')]")
        driver.execute_script("arguments[0].click();", element)
        driver.find_element_by_id("name").click()
        driver.find_element_by_id("name").clear()
        driver.find_element_by_id("name").send_keys("hi7")
        driver.find_element_by_id("birthDate").click()
        driver.find_element_by_id("birthDate").clear()
        driver.find_element_by_id("birthDate").send_keys("2000-01-07")
        driver.find_element_by_id("type").click()
        Select(driver.find_element_by_id("type")).select_by_visible_text("dog")
        driver.find_element_by_id("type").click()
        driver.find_element_by_xpath("//button[@type='submit']").click()
        element=driver.find_element_by_xpath("//a[contains(text(),'Add\n      New Pet')]")
        driver.execute_script("arguments[0].click();", element)
        driver.find_element_by_id("name").click()
        driver.find_element_by_id("name").clear()
        driver.find_element_by_id("name").send_keys("hi8")
        driver.find_element_by_id("birthDate").click()
        driver.find_element_by_id("birthDate").clear()
        driver.find_element_by_id("birthDate").send_keys("2000-01-08")
        driver.find_element_by_id("type").click()
        Select(driver.find_element_by_id("type")).select_by_visible_text("snake")
        driver.find_element_by_id("type").click()
        driver.find_element_by_xpath("//button[@type='submit']").click()
        element=driver.find_element_by_xpath("//a[contains(text(),'Add\n      New Pet')]")
        driver.execute_script("arguments[0].click();", element)
        driver.find_element_by_id("name").click()
        driver.find_element_by_id("name").clear()
        driver.find_element_by_id("name").send_keys("hi9")
        driver.find_element_by_id("birthDate").click()
        driver.find_element_by_id("birthDate").clear()
        driver.find_element_by_id("birthDate").send_keys("2000-01-09")
        driver.find_element_by_xpath("//button[@type='submit']").click()
        element=driver.find_element_by_xpath("//a[contains(text(),'Add\n      New Pet')]")
        driver.execute_script("arguments[0].click();", element)
        driver.find_element_by_id("name").click()
        driver.find_element_by_id("name").clear()
        driver.find_element_by_id("name").send_keys("hi10")
        driver.find_element_by_id("birthDate").click()
        driver.find_element_by_id("birthDate").clear()
        driver.find_element_by_id("birthDate").send_keys("2000-01-10")
        driver.find_element_by_id("type").click()
        Select(driver.find_element_by_id("type")).select_by_visible_text("cat")
        driver.find_element_by_id("type").click()
        driver.find_element_by_xpath("//button[@type='submit']").click()
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
