# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re

class PetClinic3(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(30)
        self.base_url = "https://www.google.com/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_pet_clinic3(self):
        driver = self.driver
        driver.get("http://localhost:8080/")
        driver.find_element_by_xpath("//div[@id='main-navbar']/ul/li[2]/a/span[2]").click()
        driver.find_element_by_id("lastName").click()
        driver.find_element_by_id("lastName").clear()
        driver.find_element_by_id("lastName").send_keys("Franklin")
        driver.find_element_by_xpath("//button[@type='submit']").click()
        driver.find_element_by_xpath("//a[contains(text(),'Edit\n      Owner')]").click()
        driver.find_element_by_id("firstName").click()
        driver.find_element_by_xpath("//form[@id='add-owner-form']/div/div").click()
        driver.find_element_by_id("firstName").clear()
        driver.find_element_by_id("firstName").send_keys("Alex")
        driver.find_element_by_id("address").click()
        driver.find_element_by_id("address").clear()
        driver.find_element_by_id("address").send_keys("")
        driver.find_element_by_id("address").clear()
        driver.find_element_by_id("address").send_keys("13547 abc street")
        driver.find_element_by_id("city").click()
        driver.find_element_by_id("city").clear()
        driver.find_element_by_id("city").send_keys("Newyork")
        driver.find_element_by_xpath("//form[@id='add-owner-form']/div/div[5]/div").click()
        driver.find_element_by_id("telephone").clear()
        driver.find_element_by_id("telephone").send_keys("7458936145")
        driver.find_element_by_xpath("//button[@type='submit']").click()
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
