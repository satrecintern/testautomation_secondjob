describe('PetClinic1', () => {
    context('ADD Owner기능(등록자동화)', () => {
      it('owner에 대한 정보를 입력한 후 등록', () => {
        cy.visit('http://localhost:8080/');
        cy.get('li:nth-child(2) span:nth-child(2)').click();
        cy.url().should('contains', 'http://localhost:8080/owners/find');
        cy.get('.btn:nth-child(4)').click();
        cy.url().should('contains', 'http://localhost:8080/owners/new');
        cy.get('#firstName').click();
        cy.get('#firstName').type('{backspace}');
        cy.get('#firstName').type('{backspace}');
        cy.get('#firstName').type('json');
        cy.get('#lastName').click();
        cy.get('#lastName').type('jenny');
        cy.get('#address').click();
        cy.get('#address').type('address1');
        cy.get('#city').click();
        cy.get('#city').type('seoul');
        cy.get('#telephone').click();
        cy.get('#telephone').type('01025489647');
        cy.get('.btn').click();
        //cy.get('#add-owner-form').submit();
        //cy.url().should('contains', 'http://localhost:8080/owners/11');
});
});
});
