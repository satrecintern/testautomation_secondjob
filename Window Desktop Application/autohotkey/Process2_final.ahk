﻿; This script was created using Pulover's Macro Creator
; www.macrocreator.com

#NoEnv
SetWorkingDir %A_ScriptDir%
CoordMode, Mouse, Window
SendMode Input
#SingleInstance Force
SetTitleMatchMode 2
#WinActivateForce
SetControlDelay 1
SetWinDelay 0
SetKeyDelay -1
SetMouseDelay -1
SetBatchLines -1


Macro1:
Run, C:\Program Files (x86)\Process Explorer\procexp.exe
WinActivate, Process Explorer - Sysinternals: www.sysinternals.com [DESKTOP-TNAAD23\p] ahk_class PROCEXPL
Click, 342, 22, 0
Sleep, 110
Click, 341, 22, 0
Sleep, 94
Click, 340, 22, 0
Sleep, 78
Click, 339, 23, 0
Sleep, 47
Click, 338, 24, 0
Sleep, 109
Click, 337, 25, 0
Sleep, 47
Click, 336, 25, 0
Sleep, 47
Click, 335, 25, 0
Sleep, 47
Click, 333, 27, 0
Sleep, 46
Click, 332, 28, 0
Sleep, 16
Click, 331, 28, 0
Sleep, 31
Click, 331, 29, 0
Sleep, 47
Click, 330, 30, 0
Sleep, 47
Click, 330, 31, 0
Sleep, 672
Click, 330, 32, 0
Sleep, 47
Click, 329, 33, 0
Sleep, 15
Click, 328, 33, 0
Sleep, 32
Click, 328, 34, 0
Sleep, 15
Click, 327, 35, 0
Sleep, 47
Click, 327, 36, 0
Sleep, 16
Click, 326, 36, 0
Sleep, 15
Click, 326, 37, 0
Sleep, 47
Click, 325, 38, 0
Sleep, 32
Click, 324, 39, 0
Sleep, 31
Click, 324, 41, 0
Sleep, 31
Click, 324, 42, 0
Sleep, 16
Click, 323, 43, 0
Sleep, 15
Click, 323, 44, 0
Sleep, 16
Click, 323, 45, 0
Sleep, 16
Click, 322, 46, 0
Sleep, 15
Click, 322, 47, 0
Sleep, 16
Click, 321, 48, 0
Sleep, 15
Click, 321, 50, 0
Sleep, 32
Click, 321, 51, 0
Sleep, 15
Click, 321, 53, 0
Sleep, 16
Click, 321, 54, 0
Sleep, 16
Click, 321, 55, 0
Sleep, 15
Click, 321, 56, 0
Sleep, 16
Click, 321, 57, 0
Sleep, 15
Click, 321, 58, 0
Sleep, 32
Click, 321, 59, 0
Sleep, 31
Click, 321, 61, 0
Sleep, 94
Click, 321, 62, 0
Sleep, 31
Click, 321, 64, 0
Sleep, 62
Click, 322, 66, 0
Sleep, 16
Click, 322, 67, 0
Sleep, 31
Click, 323, 69, 0
Sleep, 63
Click, 324, 69, 0
Sleep, 31
Click, 325, 71, 0
Sleep, 141
Click, 326, 70, 0
Sleep, 47
Click, 327, 69, 0
Sleep, 93
Click, 327, 68, 0
Sleep, 110
Click, 326, 68, 0
Sleep, 31
Click, 325, 67, 0
Sleep, 16
Click, 324, 66, 0
Sleep, 15
Click, 323, 65, 0
Sleep, 625
Click, 323, 65 Left, Down
Sleep, 63
Click, 1014, 78, 0
Sleep, 15
Click, 237, -212, 0
Sleep, 47
Click, 237, -212 Left, Up
Sleep, 985
Click, 236, -212, 0
Sleep, 100

#include Gdip_All.ahk
pToken := Gdip_StartUp()

pBitmap := Gdip_BitmapFromHwnd(WinExist("System Information"))
folderPath := A_ScriptDir "\ScreenShots\"
fileName :=  A_YYYY "-" A_MM "-" A_DD "_CPU_상태캡처.jpg"
saveFileTo := folderPath fileName
MsgBox % saveFileTo

Gdip_SaveBitmapToFile(pBitmap, saveFileTo)
Gdip_DisposeImage(pBitmap)

Gdip_Shutdown(pToken)
Exitapp
Return

